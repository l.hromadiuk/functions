const getSum = (str1, str2) => {
  if (isNaN(+str1 || +str2) || typeof (str1 || str2) === 'object') return false;
  return (+str1 + +str2).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var countPosts = 0;
  let posts = listOfPosts.filter(el => el.author === authorName).length;
  countPosts = posts;
  var countComments = 0;
  for (let post of listOfPosts) {
    var count = (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length)
    countComments += count;

  }


  return `Post:${countPosts},comments:${countComments}`;



};

const tickets = (people) => {
  var a25 = 0, a50 = 0;
  for (let person of people) {
    if (person == 25) {
      a25 += 1;
    }
    if (person == 50) {
      a25 -= 1; a50 += 1;
    }
    if (person == 100) {
      if (a50 == 0 && a25 >= 3) {
        a25 -= 3;
      } else {
        a25 -= 1; a50 -= 1;
      }
    }
    if (a25 < 0 || a50 < 0) {
      return 'NO';
    }
  }
  return 'YES';
};


module.exports = { getSum, getQuantityPostsByAuthor, tickets };
